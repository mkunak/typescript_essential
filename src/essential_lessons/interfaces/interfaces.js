"use strict";
function getLength(variable) {
    return variable.length;
}
function getLengthNew(variable) {
    return variable.length;
}
console.log(getLength('hello'));
console.log(getLengthNew('hello'));
console.log(getLength([true, 2, "3", { a: 4 }]));
console.log(getLengthNew([true, 2, "3", { a: 4 }]));
var box = {
    name: 'Yellow Box',
    height: 100,
    length: 80,
};
console.log(getLength(box));
console.log(getLengthNew(box));
var user11 = {
    name: 'Jackie',
    age: 48,
    logInfo: function (info) {
        console.log('Info: ', info);
    }
};
// we can implement interface into class:
var NewUser = /** @class */ (function () {
    function NewUser() {
        this.name = 'David';
        this.job = 'Front End';
        this.age = 55;
        this.birthYear = 1970;
    }
    NewUser.prototype.logInfo = function (info) {
        console.log('Info: ', info);
    };
    NewUser.prototype.getYear = function () {
        return this.birthYear;
    };
    return NewUser;
}());
var newUser = new NewUser();
newUser.logInfo('Just senseless info');
console.log('User Birth Year: ', newUser.getYear());
//# sourceMappingURL=interfaces.js.map