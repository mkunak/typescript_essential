"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// decorators
function logger(constrFn) {
    console.log(constrFn);
    // return constrFn;
}
function shouldLog(flag) {
    return flag ? logger : null;
}
var Student = /** @class */ (function () {
    function Student(firstName, secondName, age, faculty, speciality, isOnStudy) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.faculty = faculty;
        this.speciality = speciality;
        this.isOnStudy = isOnStudy;
    }
    Student = __decorate([
        shouldLog(false)
    ], Student);
    return Student;
}());
// =============================== //
function displayData(constrFn) {
    constrFn.prototype.displayHTML = function () {
        for (var key in this) {
            if (this.hasOwnProperty(key)) {
                var p = document.createElement('p');
                p.innerHTML = this[key];
                document.body.appendChild(p);
            }
        }
    };
}
var Person = /** @class */ (function () {
    function Person(firstName, secondName, age, speciality) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.speciality = speciality;
    }
    Person = __decorate([
        displayData
    ], Person);
    return Person;
}());
var person = new Person('John', 'Doe', 39, 'Full stack');
console.log(person);
person.displayHTML();
//# sourceMappingURL=decorators.js.map