// let array = [number, string] = [23, 'projects'];

let user: {
    firstName: string,
    secondName: string,
    age: number,
    isEmployed: boolean,
    skills: string[],
    getFullName: () => void
} = {
    firstName: 'John',
    secondName: 'Doe',
    age: 50,
    isEmployed: true,
    skills: ['Angular', 'React', 'NodeJS'],
    getFullName: function (): void {
        console.log(this.firstName + ' ' + this.secondName);
    }
};

type User = {
    firstName: string,
    secondName: string,
    age: number,
    isEmployed: boolean,
    skills: string[],
    getFullName: () => void,
    getAge?: () => void,
}

let user1: User = {
    firstName: 'John',
    secondName: 'Doe',
    age: 50,
    isEmployed: true,
    skills: ['Angular', 'React', 'NodeJS'],
    getFullName: function (): void {
        console.log(this.firstName + ' ' + this.secondName);
    }
};

let user2: User = {
    firstName: 'Jane',
    secondName: 'Dawson',
    age: 40,
    isEmployed: true,
    skills: ['Vue', 'React', 'Java'],
    getFullName(): void {
        console.log("User's full name: ", this.firstName + ' ' + this.secondName);
    },
    getAge(): void {
        console.log("User's age: ", this.age);
    }
};
