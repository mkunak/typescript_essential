"use strict";
// types in function, assigning by default
function sum(a, b) {
    if (b === void 0) { b = 40; }
    return a + b;
}
console.log(sum(1, 4));
console.log(sum(1));
// void
function consoleLog(str) {
    console.log(str);
}
consoleLog('Hello from consoleLog');
// assigning a function to a variable
var mySum;
mySum = sum;
console.log(mySum(10, 25));
//# sourceMappingURL=functions.js.map