"use strict";
// enum
var Job;
(function (Job) {
    Job[Job["FrontEnd"] = 0] = "FrontEnd";
    Job[Job["BackEnd"] = 1] = "BackEnd";
    Job[Job["Designer"] = 2] = "Designer";
})(Job || (Job = {}));
var job1 = Job.BackEnd;
console.log(job1);
var job2 = Job.Designer;
console.log(job2);
var Company;
(function (Company) {
    Company[Company["LuxSoft"] = 0] = "LuxSoft";
    Company[Company["Ciklum"] = 33] = "Ciklum";
    Company[Company["Infopulse"] = 34] = "Infopulse";
})(Company || (Company = {}));
var company1 = Company.Ciklum;
console.log(company1);
var company2 = Company.Infopulse;
console.log(company2);
// never
function throwError(err) {
    throw new Error(err);
}
// null
var newVar;
newVar = null;
var myNumber = 20;
myNumber = null;
//# sourceMappingURL=special_types.js.map