class Employee {
    name: string;
    isEmployed: boolean;
    age: number;

    constructor(name: string, age: number = 54, isEmployed: boolean = true) {
        this.name = name;
        this.age = age;
        this.isEmployed = isEmployed;
    }

    getAge(): number {
        return this.age;
    }
}

const employee = new Employee('John');
console.log(employee);
console.log(employee.getAge());

export class Employer {
    constructor(
        public name: string,
        protected age: number,
        private sector: string,
        private isOnMarket: true
    ) {
    }

    private getOnMarketStatus() {
        return this.isOnMarket;
    }

    public setSector(sector: string) {
        this.sector = sector;
        console.log(this.getOnMarketStatus());
    }

}

const employer = new Employer('Siemens', 154, 'Energy', true);
console.log(employer);
employer.setSector('Automation');
console.log(employer);
