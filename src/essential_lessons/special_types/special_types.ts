// enum
enum Job {
    FrontEnd,
    BackEnd,
    Designer,
}

const job1: Job = Job.BackEnd;
console.log(job1);

const job2: Job = Job.Designer;
console.log(job2);

enum Company {
    LuxSoft,
    Ciklum = 33,
    Infopulse,
}

const company1: Company = Company.Ciklum;
console.log(company1);

const company2: Company = Company.Infopulse;
console.log(company2);

// never
function throwError(err: string): never {
    throw new Error(err);
}

// null
let newVar;
newVar = null;
let myNumber: number | null = 20;
myNumber = null;
