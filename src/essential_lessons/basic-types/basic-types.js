"use strict";
// string:
var str = 'Hello World!';
// number:
var num = 10;
// boolean:
var isTrue = true;
var isFalse = false;
// any:
var anyVar = true;
anyVar = 1;
anyVar = 'Hello';
// several types:
var someVar = true;
someVar = 1;
someVar = 'Hello Any';
var error;
error = 1;
error = 'hello';
//# sourceMappingURL=basic-types.js.map