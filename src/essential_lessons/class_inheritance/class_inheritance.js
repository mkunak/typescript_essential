"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Firm = /** @class */ (function () {
    function Firm(name, sector) {
        this.name = name;
        this.sector = sector;
        this.isOnMarket = true;
        this.age = 154;
    }
    Firm.prototype.getOnMarketStatus = function () {
        return this.isOnMarket;
    };
    Firm.prototype.getAge = function () {
        return this.age;
    };
    return Firm;
}());
var company = new Firm('Siemens', 'Energy');
console.log(company);
var LuxSoft = /** @class */ (function (_super) {
    __extends(LuxSoft, _super);
    function LuxSoft(sector) {
        var _this = _super.call(this, 'LuxSoft', sector) || this;
        _this.age = 23;
        return _this;
    }
    return LuxSoft;
}(Firm));
var luxSoft = new LuxSoft('Software production');
console.log(luxSoft);
console.log(luxSoft.getAge());
// abstract
var Car = /** @class */ (function () {
    function Car(name) {
        this.year = 2008;
        this.type = 'PKW';
        this.isReady = true;
        this.name = name;
    }
    Car.prototype.setReadyStatus = function (isReady) {
        this.isReady = isReady;
    };
    return Car;
}());
var BMW = /** @class */ (function (_super) {
    __extends(BMW, _super);
    function BMW() {
        return _super.call(this, 'BMW') || this;
    }
    BMW.prototype.getFullInfo = function () {
        console.log({
            name: this.name,
            year: this.year,
            type: this.type,
            isReady: this.isReady
        });
    };
    return BMW;
}(Car));
var bmw = new BMW();
bmw.getFullInfo();
bmw.setReadyStatus(false);
bmw.getFullInfo();
//# sourceMappingURL=class_inheritance.js.map