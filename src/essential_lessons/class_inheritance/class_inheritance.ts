class Firm {
    private isOnMarket: boolean = true;
    protected age: number = 154;

    constructor(public name: string, public sector: string) {
    }

    public getOnMarketStatus(): boolean {
        return this.isOnMarket;
    }

    public getAge(): number {
        return this.age;
    }
}

const company = new Firm('Siemens', 'Energy');
console.log(company);

class LuxSoft extends Firm {
    constructor(sector: string) {
        super('LuxSoft', sector);
        this.age = 23;
    }
}

const luxSoft = new LuxSoft('Software production');
console.log(luxSoft);
console.log(luxSoft.getAge());

// abstract

abstract class Car {
    name: string;
    year: number = 2008;
    type: string = 'PKW';
    isReady: boolean = true;

    constructor(name: string) {
        this.name = name;
    }

    public setReadyStatus(isReady: boolean) {
        this.isReady = isReady;
    }

    abstract getFullInfo(): void;
}

class BMW extends Car {
    constructor() {
        super('BMW');
    }

    getFullInfo(): void {
        console.log({
            name: this.name,
            year: this.year,
            type: this.type,
            isReady: this.isReady
        })
    }
}

const bmw = new BMW();
bmw.getFullInfo();
bmw.setReadyStatus(false);
bmw.getFullInfo();
