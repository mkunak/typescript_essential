"use strict";
// array
var numArray1 = [1, 2, 3];
var numArray2 = [1, 2, 3]; // the same
var strArray1 = ['1', '2', '3'];
var strArray2 = ['1', '2', '3'];
var boolArray1 = [true, false];
var boolArray2 = [true, false];
// tuples
var arr = [true, 2, 'string'];
//# sourceMappingURL=array.js.map