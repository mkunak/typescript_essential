"use strict";
// let array = [number, string] = [23, 'projects'];
var user = {
    firstName: 'John',
    secondName: 'Doe',
    age: 50,
    isEmployed: true,
    skills: ['Angular', 'React', 'NodeJS'],
    getFullName: function () {
        console.log(this.firstName + ' ' + this.secondName);
    }
};
var user1 = {
    firstName: 'John',
    secondName: 'Doe',
    age: 50,
    isEmployed: true,
    skills: ['Angular', 'React', 'NodeJS'],
    getFullName: function () {
        console.log(this.firstName + ' ' + this.secondName);
    }
};
var user2 = {
    firstName: 'Jane',
    secondName: 'Dawson',
    age: 40,
    isEmployed: true,
    skills: ['Vue', 'React', 'Java'],
    getFullName: function () {
        console.log("User's full name: ", this.firstName + ' ' + this.secondName);
    },
    getAge: function () {
        console.log("User's age: ", this.age);
    }
};
//# sourceMappingURL=objects.js.map