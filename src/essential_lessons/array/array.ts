// array
let numArray1: number[] = [1, 2, 3];
let numArray2: Array<number> = [1, 2, 3]; // the same

let strArray1: string[] = ['1', '2', '3'];
let strArray2: Array<string> = ['1', '2', '3'];

let boolArray1: boolean[] = [true, false];
let boolArray2: Array<boolean> = [true, false];

// tuples
let arr: [boolean, number, string] = [true, 2, 'string'];
