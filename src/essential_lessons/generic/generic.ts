console.log('Generic Type Lesson:');

function getData(data: any) {
    return data;
}

console.log(getData('Some string.')); // Some string.
console.log(getData(100)); // 100
console.log(getData('Some string.').length); // 12
console.log(getData(100).length); // undefined

// generic type is a type, that we use currently. Let's discover this further:
function getDataGeneric<T>(data: T): T {
    return data;
}

console.log(getDataGeneric('Some string.')); // Some string.
console.log(getDataGeneric(100)); // 100
console.log(getDataGeneric('Some string.').length); // 12
// console.log(getDataGeneric(100).length); // error

// also we can send current data type directly to function getDataGeneric inside "<>":
console.log(getDataGeneric<string>('Some string.')); // Some string.
console.log(getDataGeneric<number>(100).toFixed(2)); // 100.00
// same thing we can saw with arrays:
const someArray: Array<number> = [1, 3, 12];

// next topic is an assign function to variables:
const genericFunc: <T> (data: T) => T = getDataGeneric;
console.log(genericFunc<string>('Some string.')); // Some string.
console.log(genericFunc<number>(100).toFixed(2)); // 100.00

// next topic. How generic type works with classes?
class Multiply<T> {
    constructor(private a: T, private b: T) {
    }

    public getResult(): number {
        return +this.a * +this.b;
    }
}

const multiplyNum = new Multiply(23, 47); // as type heir can be our Multiply class
console.log('multiplyNum: ', multiplyNum.getResult());
const multiplyStr = new Multiply('f', '47');
console.log('multiplyStr: ', multiplyStr.getResult());

// generic type can inherit from other basic type
class Minus<T extends number | string> {
    constructor(private a: T, private b: T) {
    }

    public getResult(): number {
        return +this.a - +this.b;
    }
}

const minusNum = new Minus(23, 3);
const minusNumGoodPractic = new Minus<number>(23, 3); // !very important to use this syntax if we have custom type
const minusStr = new Minus('12', '4');
const minusStrGoodPractic = new Minus<string>('12', '4'); // !...
// const minusStrNum = new Minus('12', 4); error - different types
console.log('minusStr: ', minusStrGoodPractic.getResult());
