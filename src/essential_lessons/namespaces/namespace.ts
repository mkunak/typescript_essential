console.log('Namespace:');

namespace Utils {
  export function isEmpty<T>(d: T): boolean {
    return !d;
  }

  export function isUndefined<T>(d: T): boolean {
    return typeof d === 'undefined';
  }

  export const PI = Math.PI;
  export const EXP = Math.E;
}

const EXP = 'test';

console.log(Utils.isEmpty('Hello!'));
console.log(EXP);
console.log(Utils.EXP);
