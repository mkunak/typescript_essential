// string:
let str: string = 'Hello World!';

// number:
let num: number = 10;

// boolean:
let isTrue: boolean = true;
let isFalse: boolean = false;

// any:
let anyVar: any = true;
anyVar = 1;
anyVar = 'Hello';

// several types:
let someVar: boolean | number | string = true;
someVar = 1;
someVar = 'Hello Any';

let error;
error = 1;
error = 'hello';
