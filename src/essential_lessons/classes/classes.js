"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Employee = /** @class */ (function () {
    function Employee(name, age, isEmployed) {
        if (age === void 0) { age = 54; }
        if (isEmployed === void 0) { isEmployed = true; }
        this.name = name;
        this.age = age;
        this.isEmployed = isEmployed;
    }
    Employee.prototype.getAge = function () {
        return this.age;
    };
    return Employee;
}());
var employee = new Employee('John');
console.log(employee);
console.log(employee.getAge());
var Employer = /** @class */ (function () {
    function Employer(name, age, sector, isOnMarket) {
        this.name = name;
        this.age = age;
        this.sector = sector;
        this.isOnMarket = isOnMarket;
    }
    Employer.prototype.getOnMarketStatus = function () {
        return this.isOnMarket;
    };
    Employer.prototype.setSector = function (sector) {
        this.sector = sector;
        console.log(this.getOnMarketStatus());
    };
    return Employer;
}());
exports.Employer = Employer;
var employer = new Employer('Siemens', 154, 'Energy', true);
console.log(employer);
employer.setSector('Automation');
console.log(employer);
//# sourceMappingURL=classes.js.map