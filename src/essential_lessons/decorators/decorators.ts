// decorators
function logger(constrFn: Function): void {
  console.log(constrFn);
  // return constrFn;
}

function shouldLog(flag: boolean): any {
  return flag ? logger : null
}

@shouldLog(false)
class Student {
  constructor(
    public firstName: string,
    public secondName: string,
    public age: number,
    public faculty: string,
    public speciality: string,
    public isOnStudy: boolean,
  ) {
  }
}

// =============================== //

function displayData(constrFn: Function): void {
  constrFn.prototype.displayHTML = function () {
    for (let key in this) {
      if (this.hasOwnProperty(key)) {
        const p = document.createElement('p');
        p.innerHTML = this[key];
        document.body.appendChild(p);
      }
    }
  }
}

@displayData
class Person {
  constructor(
    public firstName: string,
    public secondName: string,
    public age: number,
    public speciality: string,
  ) {
  }
}

const person = new Person('John', 'Doe', 39, 'Full stack');
console.log(person);
(<any>person).displayHTML();
