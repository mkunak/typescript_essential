// types in function, assigning by default
function sum(a: number, b: number = 40): number {
    return a + b;
}

console.log(sum(1, 4));
console.log(sum(1));

// void
function consoleLog(str: string): void {
    console.log(str);
}

consoleLog('Hello from consoleLog');

// assigning a function to a variable
let mySum: (num1: number, num2: number) => number;

mySum = sum;

console.log(mySum(10, 25));
