function getLength(variable: string | { length: number }): number {
    return variable.length;
}

// or:
interface ILength {
    length: number;
}

function getLengthNew(variable: string | ILength) {
    return variable.length;
}

console.log(getLength('hello'));
console.log(getLengthNew('hello'));
console.log(getLength([true, 2, "3", {a: 4}]));
console.log(getLengthNew([true, 2, "3", {a: 4}]));

const box = {
    name: 'Yellow Box',
    height: 100,
    length: 80, // there is key 'length' !
};

console.log(getLength(box));
console.log(getLengthNew(box));

////////////////////////////////////////////////

interface IUser {
    name: string;
    age?: number;

    logInfo(info: string): void;
}

interface IGetYear {
    getYear(): number;
}

const user11: IUser = {
    name: 'Jackie',
    age: 48,
    logInfo(info: string): void {
        console.log('Info: ', info);
    }
};

// we can implement interface into class:
class NewUser implements IUser, IGetYear {
    name: string = 'David';
    job: string = 'Front End';
    age: number = 55;
    birthYear: number = 1970;

    logInfo(info: string): void {
        console.log('Info: ', info);
    }

    getYear(): number {
        return this.birthYear;
    }
}

const newUser = new NewUser();
newUser.logInfo('Just senseless info');

console.log('User Birth Year: ', newUser.getYear());
