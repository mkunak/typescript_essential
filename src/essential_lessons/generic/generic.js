"use strict";
console.log('Generic Type Lesson:');
function getData(data) {
    return data;
}
console.log(getData('Some string.')); // Some string.
console.log(getData(100)); // 100
console.log(getData('Some string.').length); // 12
console.log(getData(100).length); // undefined
// generic type is a type, that we use currently. Let's discover this further:
function getDataGeneric(data) {
    return data;
}
console.log(getDataGeneric('Some string.')); // Some string.
console.log(getDataGeneric(100)); // 100
console.log(getDataGeneric('Some string.').length); // 12
// console.log(getDataGeneric(100).length); // error
// also we can send current data type directly to function getDataGeneric inside "<>":
console.log(getDataGeneric('Some string.')); // Some string.
console.log(getDataGeneric(100).toFixed(2)); // 100.00
// same thing we can saw with arrays:
var someArray = [1, 3, 12];
// next topic is an assign function to variables:
var genericFunc = getDataGeneric;
console.log(genericFunc('Some string.')); // Some string.
console.log(genericFunc(100).toFixed(2)); // 100.00
// next topic. How generic type works with classes?
var Multiply = /** @class */ (function () {
    function Multiply(a, b) {
        this.a = a;
        this.b = b;
    }
    Multiply.prototype.getResult = function () {
        return +this.a * +this.b;
    };
    return Multiply;
}());
var multiplyNum = new Multiply(23, 47); // as type heir can be our Multiply class
console.log('multiplyNum: ', multiplyNum.getResult());
var multiplyStr = new Multiply('f', '47');
console.log('multiplyStr: ', multiplyStr.getResult());
// generic type can inherit from other basic type
var Minus = /** @class */ (function () {
    function Minus(a, b) {
        this.a = a;
        this.b = b;
    }
    Minus.prototype.getResult = function () {
        return +this.a - +this.b;
    };
    return Minus;
}());
var minusNum = new Minus(23, 3);
var minusNumGoodPractic = new Minus(23, 3); // !very important to use this syntax if we have custom type
var minusStr = new Minus('12', '4');
var minusStrGoodPractic = new Minus('12', '4'); // !...
// const minusStrNum = new Minus('12', 4); error - different types
console.log('minusStr: ', minusStrGoodPractic.getResult());
//# sourceMappingURL=generic.js.map