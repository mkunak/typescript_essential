"use strict";
console.log('Namespace:');
var Utils;
(function (Utils) {
    function isEmpty(d) {
        return !d;
    }
    Utils.isEmpty = isEmpty;
    function isUndefined(d) {
        return typeof d === 'undefined';
    }
    Utils.isUndefined = isUndefined;
    Utils.PI = Math.PI;
    Utils.EXP = Math.E;
})(Utils || (Utils = {}));
var EXP = 'test';
console.log(Utils.isEmpty('Hello!'));
console.log(EXP);
console.log(Utils.EXP);
//# sourceMappingURL=namespace.js.map